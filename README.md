
# time manager

## Run project

```bash
# login to pull docker container (front & back)
docker login registry.gitlab.com
# run
docker-compose up
```

## Persistence of data
> The database will be in the folder `postgres-data`

### Created by

 * Sylvain Dupuy
 * Michaël Lucas 
 * David Schoffit
 * Said Mammar