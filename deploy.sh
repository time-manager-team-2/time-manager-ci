#!/bin/bash

sudo docker-compose down
sudo -E docker-compose pull
sudo -E docker-compose up -d --force-recreate --build
exit